//
// Created by Daria Supriadkina on 25.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE *out, struct image *img);


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER,
    READ_NULL
};

enum read_status from_bmp(FILE *in, struct image *img);
#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
