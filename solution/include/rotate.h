//
// Created by Daria Supriadkina on 25.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "image.h"

struct image rotate(struct image source);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
