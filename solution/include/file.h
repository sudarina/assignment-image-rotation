//
// Created by Daria Supriadkina on 25.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H

#include <stdbool.h>
#include <stdio.h>

bool file_open(FILE **f, const char *filename, const char *mode);

bool file_close(FILE *f);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
