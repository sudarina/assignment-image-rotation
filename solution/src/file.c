//
// Created by Daria Supriadkina on 25.12.2021.
//

#include "../include/file.h"


bool file_open(FILE **f, const char *fname, const char *mode){
    *f = fopen(fname, mode);
    if (*f != NULL) {
        return true;
    }
    return false;
}

bool file_close(FILE *f){
    if (fclose(f) == EOF){
        return false;
    }
    return true;
}
