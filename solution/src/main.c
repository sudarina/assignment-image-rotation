#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/rotate.h"

#include <stdio.h>

int main(int argc, char** argv){

    if(argc != 3){
        fprintf(stderr, "Invalid amount of arguments (expected 2 got %d)\n", argc);
        return 1;
    }

    FILE *in = NULL, *out = NULL;
    struct image img = {0, 0, NULL};
    struct image rotated = {0, 0, NULL};

    if(!(file_open(&in, argv[1], "rb"))) {
        fprintf(stderr, "Can't open the file for reading\n");
        return 2;
    }

    if(!(file_open(&out, argv[2], "wb"))){
        fprintf(stderr, "Can't open the file for writing\n");
        return 3;
    }


    if(from_bmp(in, &img) != READ_OK){
        fprintf(stderr, "Can't convert file fom bmp to image\n");
        return 4;
    }

    rotated = rotate(img);
    free_image(&img);

    if(to_bmp(out, &rotated) != WRITE_OK){
        fprintf(stderr, "Can't convert file to bmp\n");
        return 5;
    }

    free_image(&rotated);


    file_close(in);
    file_close(out);

    return 0;
}
