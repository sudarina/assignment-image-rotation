//
// Created by Daria Supriadkina on 25.12.2021.
//

#include "../include/image.h"
#include <stdlib.h>


uint32_t image_byte_width(struct image const* img){
    return sizeof (struct pixel) * img->width;
}

uint32_t image_byte_size(struct image const* img){
    return sizeof(struct pixel) * img->width * img->height;
}

struct pixel get_pixel(const struct image img, const uint64_t x, const uint64_t y){
    return img.data[x + y * img.width];
}

struct image* create_image(struct image* img, uint64_t width, uint64_t height){
    img->height = height;
    img->width = width;
    img->data = malloc(image_byte_size(img));
    return img;
}

void free_image(struct image *img){
    free(img->data);
}
