//
// Created by Daria Supriadkina on 25.12.2021.
//

#include "../include/rotate.h"
#include <stdlib.h>

struct image rotate(struct image source) {
    struct image rotated = {0, 0, NULL};
    rotated.width = source.height;
    rotated.height = source.width;
    rotated.data = malloc(image_byte_size(&source));

    for (size_t y = 0; y < rotated.height; y++) {
        for (size_t x = 0; x < rotated.width; x++) {
            rotated.data[y * rotated.width + x] = get_pixel(source, y, source.height - x - 1);
        }
    }
    return rotated;
}
