//
// Created by Daria Supriadkina on 25.12.2021.
//

#include "../include/bmp.h"
#include "../include/file.h"

#include <stdlib.h>

static const uint32_t bfType = 0x4D42;
static const uint32_t biSize = 40;
static const size_t biBitCount = 24;

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

size_t bmp_padding(uint64_t width){
    return 4 - (width * 3) % 4;
}

static void fill_padding(struct image *img, FILE *out){
    uint8_t padd = bmp_padding(img->width);
    uint8_t padding[4];
    for (size_t i = 0; i < padd; i++) {
        padding[i] = 0;
    }
    fwrite(padding, padd, 1, out);
}

uint32_t image_bmp_size(struct image *img){
    return (image_byte_width(img) + bmp_padding(img->width)) * img->height;
}


struct bmp_header create_header(struct image *img) {
    struct bmp_header h = {0};
    const uint32_t img_size = image_bmp_size(img);
    h.bfType = bfType;
    h.bfileSize = sizeof(struct bmp_header) + img_size;
    h.bfReserved = 0;
    h.bOffBits = sizeof(struct bmp_header);
    h.biSize = biSize;
    h.biWidth = img->width;
    h.biHeight = img->height;
    h.biPlanes = 1;
    h.biBitCount = biBitCount;
    h.biCompression = 0;
    h.biSizeImage = img_size;
    h.biXPelsPerMeter = 0;
    h.biYPelsPerMeter = 0;
    h.biClrUsed = 0;
    h.biClrImportant = 0;
    return h;
}

enum read_status from_bmp(FILE *in, struct image *img){
    struct bmp_header header = {0};
    if(fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        fprintf(stderr, "Got an error while reading header\n");
        return READ_NULL;
    }
    if (header.biBitCount != 24 || !in){
        fprintf(stderr, "Not supported bit count (not 24)\n");
        return READ_NULL;
    }
    if (header.bfType != bfType){
        return READ_INVALID_SIGNATURE;
    }

    img = create_image(img, header.biWidth, header.biHeight);

    for(int i = 0; i < img->height; i++){
        if(fread(img->data + i * img->width, image_byte_width(img), 1, in) != 1){
            fprintf(stderr, "Got an error while reading\n");
            return READ_NULL;
        }
        if (fseek(in, bmp_padding(img->width), SEEK_CUR)){
            return READ_NULL;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img){
    struct bmp_header header = create_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1) {
        return WRITE_ERROR;
    }
    fseek(out, header.bOffBits, SEEK_SET);
    if (img->data != NULL) {
        for (size_t i = 0; i < img->height; i++) {
            fwrite(img->data + i * img->width, image_byte_width(img), 1, out);
            fill_padding(img, out);
        }
    }
    return WRITE_OK;
}
